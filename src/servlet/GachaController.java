package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.GachaDAO;
import gacha.Gacha;
import gacha.GachaProcessing;
//ガチャゲームを動かすコントローラー
/**
 * Servlet implementation class GachaController
 */
@WebServlet("/GachaController")
public class GachaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//フォワード先
		String forwardPath=null;
		//actionの値をゲット
		String action =request.getParameter("action");

		//最初に立ち上げた際
		if(action==null){
			//ガチャ数値の保管クラスをインスタンス化
			Gacha gacha=new Gacha();
			//セッションインスタンスの作成
			HttpSession session=request.getSession();
			//
			//セッションスコープにインスタンスを保存
			session.setAttribute("gacha", gacha);

			forwardPath="/index.jsp";
		}
		RequestDispatcher dispatcher=request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ガチャを回すを選択した際
		HttpSession session=request.getSession();
		Gacha gacha=(Gacha)session.getAttribute("gacha");
		//ガチャを回すクラスをインスタンス化
		GachaProcessing gp=new GachaProcessing();
		gp.GachaStart(gacha);
		//データベースに即格納
		if (gacha.isCharaGetFlg() ==true) {
		GachaDAO gachaDAO=new GachaDAO();
		gachaDAO.Create(gacha);
		}

		String forwardPath="/WEB-INF/jsp/GachaResult.jsp";
		RequestDispatcher dispatcher=request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request,response);

	}

}
