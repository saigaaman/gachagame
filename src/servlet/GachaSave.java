package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.GachaDAO;
import gacha.Gacha;

/**
 * Servlet implementation class GachaSave
 */
@WebServlet("/GachaSave")
public class GachaSave extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Gacha gacha=(Gacha)session.getAttribute("gacha");
		GachaDAO gachaDAO=new GachaDAO();
		gachaDAO.findAll(gacha.gachaList);
		String forwardPath="/WEB-INF/jsp/GachaManagement.jsp";
		RequestDispatcher dispatcher=request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request,response);
	}

}
