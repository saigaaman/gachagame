package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import gacha.Gacha;
import gacha.GachaData;

public class GachaDAO {
	private final String JDBC_URL=
			"jdbc:mysql://localhost:3306/gachasave"+"?useUnicode=true&characterEncoding=utf8";
	private final String DB_USER="root";
	private final String DB_PASS="root";

	public List<GachaData>findAll(List<GachaData> gachaList){

        try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}

		//データベースへ接続
		try(Connection conn= DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){

			//SELECT文を準備
			String sql="SELECT NAME,NUM FROM DATA";
			PreparedStatement pStmt =conn.prepareStatement(sql);

			//SELECTを実行し、結果表を取得
			ResultSet rs=pStmt.executeQuery();
			//結果表に格納されたレコードの内容を
			//Employeeインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()){
				String name=rs.getString("NAME");
				int num=rs.getInt("NUM");
				GachaData gachadata=new GachaData(name,num);
				gachaList.add(gachadata);

			}



		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}

		return gachaList;
	}

	public boolean Create(Gacha gacha){
        try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}

		//データベースへ接続
		try(Connection conn= DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){


			//INSERT文を準備
			String sql="INSERT INTO DATA(NAME,NUM) VALUE(?,?)";
			PreparedStatement pStmt =conn.prepareStatement(sql);
				pStmt.setString(1,gacha.getName());
				pStmt.setInt(2,gacha.getNum());

			int result=pStmt.executeUpdate();

			if(result!=1){
				return false;
			}


		}catch(SQLException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}


}
