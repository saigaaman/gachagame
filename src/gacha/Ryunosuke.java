package gacha;

public class Ryunosuke extends Player{
	//コンストラクタ
	public Ryunosuke(){
		this.setNum(3);
		this.setRarity("★★★★☆");//キャラのレアリティ。★で表記
		this.setName("龍之介");//キャラネーム
		this.setGender("男");
		this.setType("遠距離・破壊タイプ");//キャラタイプ
		this.setAttribute("雷");//キャラの属性
		String status[]=
			{"A","C","B","A","D","D","C","D","A"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【貫通弾】");
		this.setSkill1Explanation("対象に防御力無視の中ダメージを与える");
		this.setSkill2Name("【徹甲弾】");
		this.setSkill2Explanation("対象の装甲値を大きく削り取る銃弾を放つ");
		this.setSkill3Name("【破壊弾】");
		this.setSkill3Explanation("対象とその周囲に大ダメージを与え、防御力を下げる（小）");
		this.setCharacteristic("【破壊王】自身が対象の装甲を破壊すれば、３ターンの耐性無視スタン状態を与える");//キャラの特性
		this.setCharaDescription("主人公の兄");//キャラの説明
		this.setScript("おう。");
	}
}
