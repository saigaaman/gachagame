package gacha;

public class Kyal extends Player{
	//コンストラクタ
	public Kyal(){
		this.setNum(8);
		this.setRarity("★★★☆☆");//キャラのレアリティ。★で表記
		this.setName("キャル");//キャラネーム
		this.setGender("女");
		this.setType("遠距離・魔法タイプ");//キャラタイプ
		this.setAttribute("闇");//キャラの属性
		String status[]=
			{"D","A","E","E","A","B","B","E","A"};
		this.setSkill1Name("【グリムバースト】");
		this.setSkill1Explanation("敵全体に魔法中ダメージを与える");
		this.setSkill2Name("【サンダーボール】");
		this.setSkill2Explanation("目の前の敵１キャラに魔法中ダメージを与える");
		this.setSkill3Name("【アーマーダウン】");
		this.setSkill3Explanation("目の前の敵１キャラの物理防御力と魔法防御力を小ダウンさせる");
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setCharacteristic("【ダークエクリプス】ターン経過ごとに自身の魔力上昇（最大1.5倍）");//キャラの特性
		this.setCharaDescription("プリンセスコネクトに登場するヒロインの一人");//キャラの説明
		this.setScript("やばいわよ！");
	}
}



