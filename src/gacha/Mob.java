package gacha;

//モブプレイヤー。いわゆるハズレ
public class Mob extends Player{
	public Mob(){
	this.setNum(9);
	this.setRarity("★☆☆☆☆");//キャラのレアリティ。★で表記
	this.setName("モブ");//キャラネーム
	this.setGender("女");
	this.setType("近距離・パワータイプ");//キャラタイプ
	this.setAttribute("無");//キャラの属性
	String status[]=
		{"E","E","E","E","E","E","E","E","E"};
	this.setSkill1Name("【なし】");
	this.setSkill1Explanation("なし");
	this.setSkill2Name("【なし】");
	this.setSkill2Explanation("なし");
	this.setSkill3Name("【なし】");
	this.setSkill3Explanation("なし");
	//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
	this.setCharaDescription("モブは所持出来ません。");//キャラの説明
	this.setScript("残念！ただのモブでした！");
	}
}
