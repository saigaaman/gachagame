package gacha;

public class Siro extends Player{
	//コンストラクタ
	public Siro(){
		this.setNum(2);
		this.setRarity("★★★☆☆");//キャラのレアリティ。★で表記
		this.setName("シロ");//キャラネーム
		this.setGender("女");
		this.setType("オールレンジ・支援タイプ");//キャラタイプ
		this.setAttribute("地");//キャラの属性
		String status[]=
			{"D","A","D","D","A","A","A","B","A"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【甘える】");
		this.setSkill1Explanation("指定した対象を回復（大）");
		this.setSkill2Name("【鳴く】");
		this.setSkill2Explanation("敵全体の攻撃力を下げ（中）、味方全体にリラックス効果（HP・SP自動回復）を与える");
		this.setSkill3Name("【起こす】");
		this.setSkill3Explanation("戦闘不能となった対象のHPを全回復する");
		this.setCharacteristic("【オートバリア】一定時間ごとに全攻撃を吸収する障壁を、自身に張る");//キャラの特性
		this.setCharaDescription("主人公の飼い猫。回復スキルにおいては他の追従を許さない性能");//キャラの説明
		this.setScript("よろしくニャ。");

	}
}

