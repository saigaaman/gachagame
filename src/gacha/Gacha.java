package gacha;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//ガチャのステータス（数値）を持つクラス
public class Gacha implements Serializable{
	private boolean charaGetFlg=true;
	private int num=0;//キャラの番号
	private String rarity;//キャラのレアリティ。★で表記
	private String name;//キャラネーム
	private String type;//キャラタイプ
	private String attribute;//キャラの属性
	public String status[]=new String[9];//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
	private String skill1Name;//スキルの名前
	private String skill1Explanation;//スキルの説明
	private String skill2Name;
	private String skill2Explanation;
	private String skill3Name;
	private String skill3Explanation;
	private String characteristic;//キャラの特性
	private String charaDescription;//キャラの説明　
	private String script;
	public ArrayList<Player> charaKeep=new ArrayList<>();//キャラは番号でキープ
	public List<GachaData>gachaList=new ArrayList<>();





	//キャラを補完するには？



	//コンストラクタ
	public Gacha(){

	}

	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getRarity() {
		return rarity;
	}
	public void setRarity(String rarity) {
		this.rarity = rarity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String[] getStatus() {
		return status;
	}
	public void setStatus(String status[]) {
		this.status = status;
	}

	public String getCharaDescription() {
		return charaDescription;
	}
	public void setCharaDescription(String charaDescription) {
		this.charaDescription = charaDescription;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getCharacteristic() {
		return characteristic;
	}

	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}

	public boolean isCharaGetFlg() {
		return charaGetFlg;
	}

	public void setCharaGetFlg(boolean charaGetFlg) {
		this.charaGetFlg = charaGetFlg;
	}

	public String getSkill1Name() {
		return skill1Name;
	}

	public void setSkill1Name(String skill1Name) {
		this.skill1Name = skill1Name;
	}

	public String getSkill1Explanation() {
		return skill1Explanation;
	}

	public void setSkill1Explanation(String skill1Explanation) {
		this.skill1Explanation = skill1Explanation;
	}

	public String getSkill2Name() {
		return skill2Name;
	}

	public void setSkill2Name(String skill2Name) {
		this.skill2Name = skill2Name;
	}

	public String getSkill2Explanation() {
		return skill2Explanation;
	}

	public void setSkill2Explanation(String skill2Explanation) {
		this.skill2Explanation = skill2Explanation;
	}

	public String getSkill3Name() {
		return skill3Name;
	}

	public void setSkill3Name(String skill3Name) {
		this.skill3Name = skill3Name;
	}

	public String getSkill3Explanation() {
		return skill3Explanation;
	}

	public void setSkill3Explanation(String skill3Explanation) {
		this.skill3Explanation = skill3Explanation;
	}










}
