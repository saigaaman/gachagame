package gacha;

public class Peko extends Player{
	//コンストラクタ
	public Peko(){
		this.setNum(6);
		this.setRarity("★★★☆☆");//キャラのレアリティ。★で表記
		this.setName("ペコリーヌ");//キャラネーム
		this.setGender("女");
		this.setType("近接・防御タイプ");//キャラタイプ
		this.setAttribute("風");//キャラの属性
		String status[]=
			{"A","C","C","A","B","A","B","D","B"};
		this.setSkill1Name("【プリンセスストライク】");
		this.setSkill1Explanation("敵単体に物理大ダメージ＋自分の物理防御力を中アップ＋自分の魔法防御力を中アップ");
		this.setSkill2Name("【ランチタイム】");
		this.setSkill2Explanation("自分の魔法防御力を中アップさせ、さらにHPを中回復する");
		this.setSkill3Name("【フォールスラッシュ】");
		this.setSkill3Explanation("目の前の敵１キャラに物理中ダメージを与える");
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setCharacteristic("【プリンセスフォーム】一定時間、自身の攻防大アップ");//キャラの特性
		this.setCharaDescription("プリンセスコネクトに登場するヒロインの一人");//キャラの説明
		this.setScript("やばいですね☆");
	}
}

