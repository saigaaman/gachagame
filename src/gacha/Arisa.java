package gacha;

public class Arisa extends Player{
	//コンストラクタ
	public Arisa(){
		this.setNum(1);
		this.setRarity("★★★★★");//キャラのレアリティ。★で表記
		this.setName("愛莉紗");//キャラネーム
		this.setGender("女");
		this.setType("近接・バランスタイプ");//キャラタイプ
		this.setAttribute("風");//キャラの属性
		String status[]=
			{"B","B","A","C","B","B","S","A","A"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【風刃】");
		this.setSkill1Explanation("敵全体に中ダメージを浴びせ、ノックバック効果（中）と遅延効果（小）を与える");
		this.setSkill2Name("【涼風】");
		this.setSkill2Explanation("味方全体を中回復し、行動を早める（小）");
		this.setSkill3Name("【羅刹】");
		this.setSkill3Explanation("向けられた攻撃を必ず回避し、反撃を放つ（ダメージ特大・２回まで）。");
		this.setCharacteristic("【完璧無欠・神速メイド】全状態異常無効+行動後に一定確率で連続行動可能");//キャラの特性
		this.setCharaDescription("主人公のメイド。高水準のステータスとスキルを持つオールラウンダー");//キャラの説明
		this.setScript("お待たせ致しました。いざ、参りましょう！");
	}
}



