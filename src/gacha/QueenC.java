package gacha;

public class QueenC extends Player{
	//コンストラクタ
	public QueenC(){
		this.setNum(4);
		this.setRarity("★★★★★");//キャラのレアリティ。★で表記
		this.setName("クイン・シー");//キャラネーム
		this.setGender("女");
		this.setType("遠距離・スキルタイプ");//キャラタイプ
		this.setAttribute("闇");//キャラの属性
		String status[]=
			{"B","B","A","C","C","C","A","A","S"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【ロード・パニッシュ】");
		this.setSkill1Explanation("ランダムの敵一体に3倍ダメージを与え、行動を遅らせる（中）");
		this.setSkill2Name("【ライトニング・ストライク】");
		this.setSkill2Explanation("敵全体に大ダメージを与え、恐慌状態を付与する（100%）");
		this.setSkill3Name("【オーバー・ライズ】");
		this.setSkill3Explanation("自身の幸運を50%上昇・その他のステータスを20%上昇（5ターン）");
		this.setCharacteristic("【英雄の加護】状態異常無効+自身の攻撃がクリティカルでダメージさらに3倍");//キャラの特性
		this.setCharaDescription("スナイパー。");//キャラの説明
		this.setScript("うん。");
	}
}
