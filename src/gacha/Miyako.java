package gacha;

public class Miyako extends Player{
	//コンストラクタ
	public Miyako(){
		this.setNum(5);
		this.setRarity("★★☆☆☆");//キャラのレアリティ。★で表記
		this.setName("ミヤコ");//キャラネーム
		this.setGender("女");
		this.setType("近接・防御タイプ");//キャラタイプ
		this.setAttribute("地");//キャラの属性
		String status[]=
			{"A","D","D","S","D","E","B","S","C"};
		this.setSkill1Name("【プリンにしてやるの】");
		this.setSkill1Explanation("敵一体にダメージを与え、自分のHPを回復（中）");
		this.setSkill2Name("【う～ら～め～し～や～なの】");
		this.setSkill2Explanation("幽霊に変身し、しばらくの間無敵状態になる");
		this.setSkill3Name("【おやつの時間なの】");
		this.setSkill3Explanation("自分のHPを中回復する");
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setCharacteristic("【幽体】自身への全ての攻撃を確率で回避");//キャラの特性
		this.setCharaDescription("プリンセスコネクトに登場する幽霊");//キャラの説明
		this.setScript("オマエもプリンにしてやるの～");
	}
}

