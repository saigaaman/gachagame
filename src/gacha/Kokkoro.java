package gacha;

public class Kokkoro extends Player{
	//コンストラクタ
	public Kokkoro(){
		this.setNum(7);
		this.setRarity("★★★☆☆");//キャラのレアリティ。★で表記
		this.setName("コッコロ");//キャラネーム
		this.setGender("女");
		this.setType("近接・補助タイプ");//キャラタイプ
		this.setAttribute("水");//キャラの属性
		String status[]=
			{"B","B","C","B","B","C","A","C","S"};
		this.setSkill1Name("【オーロラ】");
		this.setSkill1Explanation("味方全体に物理攻撃力と魔法攻撃力を小アップさせ、さらに 自分のHPを中回復させる");
		this.setSkill2Name("【トライスラッシュ】");
		this.setSkill2Explanation("目の前の敵１キャラに物理中ダメージを与える");
		this.setSkill3Name("【スピードアップ】");
		this.setSkill3Explanation("味方全体の行動速度を小アップさせ、さらに物理攻撃力を小アップさせる");
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setCharacteristic("【エール】味方全員の速度を一定時間上昇（中）");//キャラの特性
		this.setCharaDescription("プリンセスコネクトに登場するヒロインの一人");//キャラの説明
		this.setScript("主さま、やっと会えました");
	}
}


