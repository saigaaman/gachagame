package gacha;

public class Koichi extends Player{

	//コンストラクタ
	public Koichi(){
		this.setNum(0);
		this.setRarity("★★★☆☆");//キャラのレアリティ。★で表記
		this.setName("幸一");//キャラネーム
		this.setGender("男");
		this.setType("近接・スキルタイプ");//キャラタイプ
		this.setAttribute("火");//キャラの属性
		String status[]=
			{"B","C","B","B","D","C","B","C","A"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【絶影】");
		this.setSkill1Explanation("敵単体に中ダメージ、確率でスタン状態を付与する");
		this.setSkill2Name("【発破衝】");
		this.setSkill2Explanation("単体に大ダメージ、その周囲に中ダメージ、確率で炎上状態にする");
		this.setSkill3Name("【煉獄】");
		this.setSkill3Explanation("単体目標にラッシュを浴びせる連続攻撃。対象の防御力が低ければ追加ダメージ（大）");
		this.setCharacteristic("【指揮能力A】戦闘に参加するとPTメンバーの全ステータス中アップ");//キャラの特性
		this.setCharaDescription("主人公。平均的なステータスだが豊富な攻撃スキルを持つ");//キャラの説明　
		this.setScript("よし、行こうか！");
	}
	/*
	 *
	 * 	public BlackKnight(){
		this.setNum(11);
		this.setRarity("★★★★★");//キャラのレアリティ。★で表記
		this.setName("黒騎士");//キャラネーム
		this.setGender("男");
		this.setType("近接・バランスタイプ");//キャラタイプ
		this.setAttribute("闇");//キャラの属性
		String status[]=
			{"S","B","A","A","B","A","B","C","S"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【メイルシュトローム】");
		this.setSkill1Explanation("敵全体に大ダメージ+一定確率で鈍足状態");
		this.setSkill2Name("【ブラッディクロス】");
		this.setSkill2Explanation("単体に特大ダメージ+高確率でスタン");
		this.setSkill3Name("【クロノロアー】");
		this.setSkill3Explanation("敵単体に爆弾をしかける。5カウント後、敵単体に極大ダメージ+クリティカルで強制アーマーブレイク");
		this.setCharacteristic("【神器の適合者】自身の全ての攻撃に30%の追加攻撃・全ての被攻撃を30%軽減");//キャラの特性
		this.setCharaDescription("黒騎士。一時的に加入する。");//キャラの説明　
		this.setScript("フン・・・");
	}

	 *
	 *	 * 	public Chaos(){
		this.setNum(12);
		this.setRarity("★★★★★");//キャラのレアリティ。★で表記
		this.setName("ケイオス");//キャラネーム
		this.setGender("男");
		this.setType("オールレンジ・スキルタイプ");//キャラタイプ
		this.setAttribute("氷");//キャラの属性
		String status[]=
			{"C","A","C","B","S","A","B","B","S"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【】");
		this.setSkill1Explanation("");
		this.setSkill2Name("【】");
		this.setSkill2Explanation("");
		this.setSkill3Name("【】");
		this.setSkill3Explanation("");
		this.setCharacteristic("【トリックスター】全ての攻撃に特殊回避判定+沈黙・混乱・スタン状態にかからない");//キャラの特性
		this.setCharaDescription("大犯罪者の一人。");//キャラの説明　
		this.setScript("ほほう、私を入れるとな？");
	}
	 *
	 *
	 *	 *	 * 	public Master(){
		this.setNum(13);
		this.setRarity("★★★★★");//キャラのレアリティ。★で表記
		this.setName("マスター");//キャラネーム
		this.setGender("男");
		this.setType("オールレンジ・スキルタイプ");//キャラタイプ
		this.setAttribute("闇");//キャラの属性
		String status[]=
			{"A","A","S","A","C","B","S","S","S"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【シャドウゲイト】");
		this.setSkill1Explanation("敵単体に回避不能の一撃を見舞う。一般モンスターは強制即死・ボスモンスターは5倍ダメージ");
		this.setSkill2Name("【ヴァリアントナイフ】");
		this.setSkill2Explanation("敵全体に連続攻撃を与え、猛毒を付与する（100%）");
		this.setSkill3Name("【アサシンチャージ】");
		this.setSkill3Explanation("次の攻撃が必中・確定クリティカルとなる");
		this.setCharacteristic("【闇の支配者】全状態異常無効・範囲攻撃の対象にならない");//キャラの特性
		this.setCharaDescription("あらゆるアサシンの頂点・アサシンマスター。");//キャラの説明　
		this.setScript("こいつぁ、参ったな・・・。");
	}
	 *
	 *
	 *	 *	 *	 * 	public Erena(){
		this.setNum(13);
		this.setRarity("★★★★★");//キャラのレアリティ。★で表記
		this.setName("エリーナ");//キャラネーム
		this.setGender("女");
		this.setType("オールレンジ・スキルタイプ");//キャラタイプ
		this.setAttribute("水");//キャラの属性
		String status[]=
			{"B","B","A","B","A","A","A","S","S"};
		//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキルのE～S
		this.setSkill1Name("【スプラッシャー】");
		this.setSkill1Explanation("敵全体に鎌の刃風を見舞う。一般モンスターは強制即死・ボスモンスターは3倍ダメージ");
		this.setSkill2Name("【エクスキューショナー】");
		this.setSkill2Explanation("敵単体に20倍ダメージ・戦闘中一回だけ");
		this.setSkill3Name("【カオティックチャージ】");
		this.setSkill3Explanation("３ターンの間、全ての攻撃を80%軽減する。効果が切れると1ターンの間強制スタン状態となる");
		this.setCharacteristic("【闇を継ぐ者】全状態異常無効・自身を戦闘不能にした敵に呪い状態を与える");//キャラの特性
		this.setCharaDescription("アサシンマスターの実の娘にして、次期アサシンマスター。");//キャラの説明　
		this.setScript("いいわよぉ・・・");
	}
	 *
	 *
	 *
	 * */


}
