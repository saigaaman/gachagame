package gacha;

//ガチャした際の内部処理をするクラス
public class GachaProcessing {

	// ガチャで回した結果をガチャインスタンス内クラスに割り当て
	public Gacha GachaStart(Gacha gacha) {
		gacha.setCharaGetFlg(true);
		// ガチャの結果を番号として引き渡し
		int random=(new java.util.Random().nextInt(15));
		gacha.setNum(random);

		// 出たキャラをインスタンス化して格納
		switch (gacha.getNum()) {
		case 0:
			Koichi koichi = new Koichi();// インスタンス化
			gacha.setNum(koichi.getNum());// キャラ番号
			gacha.setRarity(koichi.getRarity());// レアリティ
			gacha.setName(koichi.getName());// 名前
			gacha.setType(koichi.getType());// タイプ
			gacha.setAttribute(koichi.getAttribute());// 属性
			//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "B";
			gacha.status[1] = "C";
			gacha.status[2] = "B";
			gacha.status[3] = "B";
			gacha.status[4] = "D";
			gacha.status[5] = "C";
			gacha.status[6] = "B";
			gacha.status[7] = "C";
			gacha.status[8] = "A";
			gacha.setSkill1Name(koichi.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(koichi.getSkill1Explanation());
			gacha.setSkill2Name(koichi.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(koichi.getSkill2Explanation());
			gacha.setSkill3Name(koichi.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(koichi.getSkill3Explanation());

			gacha.setCharacteristic(koichi.getCharacteristic());
			gacha.setCharaDescription(koichi.getCharaDescription());
			gacha.setScript(koichi.getScript());
			//すでに持っているキャラクターかチェック
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(koichi);
			}
			break;
		case 1:
			Arisa arisa = new Arisa();// インスタンス化
			gacha.setNum(arisa.getNum());// キャラ番号
			gacha.setRarity(arisa.getRarity());// レアリティ
			gacha.setName(arisa.getName());// 名前
			gacha.setType(arisa.getType());// タイプ
			gacha.setAttribute(arisa.getAttribute());// 属性
			//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "B";
			gacha.status[1] = "B";
			gacha.status[2] = "A";
			gacha.status[3] = "C";
			gacha.status[4] = "B";
			gacha.status[5] = "B";
			gacha.status[6] = "S";
			gacha.status[7] = "A";
			gacha.status[8] = "A";
			gacha.setSkill1Name(arisa.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(arisa.getSkill1Explanation());
			gacha.setSkill2Name(arisa.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(arisa.getSkill2Explanation());
			gacha.setSkill3Name(arisa.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(arisa.getSkill3Explanation());
			gacha.setCharacteristic(arisa.getCharacteristic());
			gacha.setCharaDescription(arisa.getCharaDescription());
			gacha.setScript(arisa.getScript());
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(arisa);
			}
			break;
		case 2:
			Siro siro = new Siro();// インスタンス化
			gacha.setNum(siro.getNum());// キャラ番号
			gacha.setRarity(siro.getRarity());// レアリティ
			gacha.setName(siro.getName());// 名前
			gacha.setType(siro.getType());// タイプ
			gacha.setAttribute(siro.getAttribute());// 属性
			//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "D";
			gacha.status[1] = "A";
			gacha.status[2] = "D";
			gacha.status[3] = "D";
			gacha.status[4] = "A";
			gacha.status[5] = "A";
			gacha.status[6] = "A";
			gacha.status[7] = "B";
			gacha.status[8] = "B";
			gacha.setSkill1Name(siro.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(siro.getSkill1Explanation());
			gacha.setSkill2Name(siro.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(siro.getSkill2Explanation());
			gacha.setSkill3Name(siro.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(siro.getSkill3Explanation());
			gacha.setCharacteristic(siro.getCharacteristic());
			gacha.setCharaDescription(siro.getCharaDescription());
			gacha.setScript(siro.getScript());
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(siro);
			}
			break;
		case 3:
			Ryunosuke ryunosuke = new Ryunosuke();// インスタンス化
			gacha.setNum(ryunosuke.getNum());// キャラ番号
			gacha.setRarity(ryunosuke.getRarity());// レアリティ
			gacha.setName(ryunosuke.getName());// 名前
			gacha.setType(ryunosuke.getType());// タイプ
			gacha.setAttribute(ryunosuke.getAttribute());// 属性
			//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "A";
			gacha.status[1] = "C";
			gacha.status[2] = "B";
			gacha.status[3] = "A";
			gacha.status[4] = "D";
			gacha.status[5] = "D";
			gacha.status[6] = "C";
			gacha.status[7] = "D";
			gacha.status[8] = "A";
			gacha.setSkill1Name(ryunosuke.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(ryunosuke.getSkill1Explanation());
			gacha.setSkill2Name(ryunosuke.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(ryunosuke.getSkill2Explanation());
			gacha.setSkill3Name(ryunosuke.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(ryunosuke.getSkill3Explanation());

			gacha.setCharacteristic(ryunosuke.getCharacteristic());
			gacha.setCharaDescription(ryunosuke.getCharaDescription());
			gacha.setScript(ryunosuke.getScript());
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(ryunosuke);
			}
			break;
		case 4:
			QueenC queenC = new QueenC();// インスタンス化
			gacha.setNum(queenC.getNum());// キャラ番号
			gacha.setRarity(queenC.getRarity());// レアリティ
			gacha.setName(queenC.getName());// 名前
			gacha.setType(queenC.getType());// タイプ
			gacha.setAttribute(queenC.getAttribute());
			//属性//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "B";
			gacha.status[1] = "B";
			gacha.status[2] = "A";
			gacha.status[3] = "C";
			gacha.status[4] = "C";
			gacha.status[5] = "C";
			gacha.status[6] = "A";
			gacha.status[7] = "A";
			gacha.status[8] = "S";
			gacha.setSkill1Name(queenC.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(queenC.getSkill1Explanation());
			gacha.setSkill2Name(queenC.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(queenC.getSkill2Explanation());
			gacha.setSkill3Name(queenC.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(queenC.getSkill3Explanation());

			gacha.setCharacteristic(queenC.getCharacteristic());
			gacha.setCharaDescription(queenC.getCharaDescription());
			gacha.setScript(queenC.getScript());
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(queenC);
			}
			break;

		case 5:
			Miyako miyako = new Miyako();// インスタンス化
			gacha.setNum(miyako.getNum());// キャラ番号
			gacha.setRarity(miyako.getRarity());// レアリティ
			gacha.setName(miyako.getName());// 名前
			gacha.setType(miyako.getType());// タイプ
			gacha.setAttribute(miyako.getAttribute());
			//属性//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "A";
			gacha.status[1] = "D";
			gacha.status[2] = "D";
			gacha.status[3] = "S";
			gacha.status[4] = "D";
			gacha.status[5] = "E";
			gacha.status[6] = "B";
			gacha.status[7] = "S";
			gacha.status[8] = "C";
			gacha.setSkill1Name(miyako.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(miyako.getSkill1Explanation());
			gacha.setSkill2Name(miyako.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(miyako.getSkill2Explanation());
			gacha.setSkill3Name(miyako.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(miyako.getSkill3Explanation());

			gacha.setCharacteristic(miyako.getCharacteristic());
			gacha.setCharaDescription(miyako.getCharaDescription());
			gacha.setScript(miyako.getScript());
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(miyako);
			}
			break;

		case 6:
			Peko peko = new Peko();// インスタンス化
			gacha.setNum(peko.getNum());// キャラ番号
			gacha.setRarity(peko.getRarity());// レアリティ
			gacha.setName(peko.getName());// 名前
			gacha.setType(peko.getType());// タイプ
			gacha.setAttribute(peko.getAttribute());
			//属性//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "A";
			gacha.status[1] = "C";
			gacha.status[2] = "C";
			gacha.status[3] = "A";
			gacha.status[4] = "B";
			gacha.status[5] = "A";
			gacha.status[6] = "B";
			gacha.status[7] = "D";
			gacha.status[8] = "B";

			gacha.setSkill1Name(peko.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(peko.getSkill1Explanation());
			gacha.setSkill2Name(peko.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(peko.getSkill2Explanation());
			gacha.setSkill3Name(peko.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(peko.getSkill3Explanation());

			gacha.setCharacteristic(peko.getCharacteristic());
			gacha.setCharaDescription(peko.getCharaDescription());
			gacha.setScript(peko.getScript());
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(peko);
			}
			break;



		case 7:
			Kokkoro kokkoro = new Kokkoro();// インスタンス化
			gacha.setNum(kokkoro.getNum());// キャラ番号
			gacha.setRarity(kokkoro.getRarity());// レアリティ
			gacha.setName(kokkoro.getName());// 名前
			gacha.setType(kokkoro.getType());// タイプ
			gacha.setAttribute(kokkoro.getAttribute());
			//属性//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "B";
			gacha.status[1] = "B";
			gacha.status[2] = "C";
			gacha.status[3] = "B";
			gacha.status[4] = "B";
			gacha.status[5] = "C";
			gacha.status[6] = "A";
			gacha.status[7] = "C";
			gacha.status[8] = "S";
			gacha.setSkill1Name(kokkoro.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(kokkoro.getSkill1Explanation());
			gacha.setSkill2Name(kokkoro.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(kokkoro.getSkill2Explanation());
			gacha.setSkill3Name(kokkoro.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(kokkoro.getSkill3Explanation());

			gacha.setCharacteristic(kokkoro.getCharacteristic());
			gacha.setCharaDescription(kokkoro.getCharaDescription());
			gacha.setScript(kokkoro.getScript());
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(kokkoro);
			}
			break;

		case 8:
			Kyal kyal = new Kyal();// インスタンス化
			gacha.setNum(kyal.getNum());// キャラ番号
			gacha.setRarity(kyal.getRarity());// レアリティ
			gacha.setName(kyal.getName());// 名前
			gacha.setType(kyal.getType());// タイプ
			gacha.setAttribute(kyal.getAttribute());
			//属性//キャラステータス。HP、SP、攻撃力、防御力、魔力、魔防、速度、回避、スキル
			gacha.status[0] = "D";
			gacha.status[1] = "A";
			gacha.status[2] = "E";
			gacha.status[3] = "E";
			gacha.status[4] = "A";
			gacha.status[5] = "B";
			gacha.status[6] = "B";
			gacha.status[7] = "E";
			gacha.status[8] = "A";
			gacha.setSkill1Name(kyal.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(kyal.getSkill1Explanation());
			gacha.setSkill2Name(kyal.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(kyal.getSkill2Explanation());
			gacha.setSkill3Name(kyal.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(kyal.getSkill3Explanation());

			gacha.setCharacteristic(kyal.getCharacteristic());
			gacha.setCharaDescription(kyal.getCharaDescription());
			gacha.setScript(kyal.getScript());
			for(int i=0;i<gacha.charaKeep.size();i++){
				//キャラキープ内のキャラ番号と
				if(random==gacha.charaKeep.get(i).getNum()){
					gacha.setCharaGetFlg(false);
				}
			}
			if(gacha.isCharaGetFlg()==true){
				gacha.charaKeep.add(kyal);
			}
			break;


		default:
			Mob mob = new Mob();// インスタンス化
			gacha.setNum(mob.getNum());// キャラ番号
			gacha.setRarity(mob.getRarity());// レアリティ
			gacha.setName(mob.getName());// 名前
			gacha.setType(mob.getType());// タイプ
			gacha.setAttribute(mob.getAttribute());// 属性
			gacha.status[0] = "E";
			gacha.status[1] = "E";
			gacha.status[2] = "E";
			gacha.status[3] = "E";
			gacha.status[4] = "E";
			gacha.status[5] = "E";
			gacha.status[6] = "E";
			gacha.status[7] = "E";
			gacha.status[8] = "E";
			gacha.setSkill1Name(mob.getSkill1Name());//スキルの名前
			gacha.setSkill1Explanation(mob.getSkill1Explanation());
			gacha.setSkill2Name(mob.getSkill2Name());//スキルの名前
			gacha.setSkill2Explanation(mob.getSkill2Explanation());
			gacha.setSkill3Name(mob.getSkill3Name());//スキルの名前
			gacha.setSkill3Explanation(mob.getSkill3Explanation());
			gacha.setCharaDescription(mob.getCharaDescription());
			gacha.setScript(mob.getScript());
			gacha.setCharaGetFlg(false);

			break;

		}
		return gacha;

	}

}
