<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="gacha.Gacha"%>
<%
	Gacha gacha = (Gacha) session.getAttribute("gacha");
%>

<!DOCTYPE html >
<html>
<head>
<style>
h1 {
	text-align: center;
	color:blue;
}
p {
	text-align : center;
	font-size: 25px;
	color: red;
}

chara {

}

maintext {

}

button {

}

body {
	background: #eeeeee;
	text-align:center;
}

.css-fukidashi1 {
	text-align:center;
	padding:0;
	margin:0;
}
.css-fukidashi2 {
	text-align:center;
	padding:0;
	margin:0;
}
.css-fukidashi3 {
	text-align:center;
	padding:0;
	margin:0;
}

.text1 {
	text-align:center;
	width: 200px;
	position:relative;
	margin: 50px auto;
	padding: 20px;
	border: 1px solid #ccc;
}
.text2 {
	text-align:center;
	width: 200px;
	position: relative;
	margin: 50px auto;
	padding: 20px;
	border: 1px solid #ccc;
}
.text3 {
	text-align:center;
	width: 200px;
	position: relative;
	margin:50px auto;
	padding: 20px;
	border: 1px solid #ccc;
}

.fukidashi1 {
	display: none;
	width: 200px;
	position: absolute;
	top: 700px;
	left: 550px;
	padding: 16px;
	border-radius: 5px;
	background: #33cc99;
	color: #fff;
	font-weight: bold;
}

.fukidashi1:after {
	position: absolute;
	width: 0;
	height: 0;
	bottom: -19px;
	border: solid transparent;
	border-color: rgba(51, 204, 153, 0);
	border-top-color: #33cc99;
	border-width: 10px;
	pointer-events: none;
	content: "";
}

.text1:hover+.fukidashi1 {
	display: block;
}

.fukidashi2 {
	display: none;
	width: 200px;
	position: absolute;
	top: 850px;
	left: 550px;
	padding: 16px;
	border-radius: 5px;
	background: #33cc99;
	color: #fff;
	font-weight: bold;
}

.fukidashi2:after {
	position: absolute;
	width: 0;
	height: 0;
	bottom: -19px;
	border: solid transparent;
	border-color: rgba(51, 204, 153, 0);
	border-top-color: #33cc99;
	border-width: 10px;
	pointer-events: none;
	content: "";
}

.text2:hover+.fukidashi2 {
	display: block;
}

.fukidashi3 {
	display: none;
	width: 200px;
	position: absolute;
	top: 980px;
	left: 550px;
	padding: 16px;
	border-radius: 5px;
	background: #33cc99;
	color: #fff;
	font-weight: bold;
}

.fukidashi3:after {
	position: absolute;
	width: 0;
	height: 0;
	bottom: -19px;
	border: solid transparent;
	border-color: rgba(51, 204, 153, 0);
	border-top-color: #33cc99;
	border-width: 10px;
	pointer-events: none;
	content: "";
}

.text3:hover+.fukidashi3 {
	display: block;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ガチャ結果</title>
</head>
<body>

	<%
		if (gacha.isCharaGetFlg() == false&&gacha.getNum()!=9) {
	%>
	<h1>残念、キャラがかぶりました！</h1>

	<%
		} else if (gacha.getNum() == 9) {
	%>
	<h1>ハズレ！</h1>
	<%
		} else {
	%>
	<h1>素敵な仲間が増えましたよ！</h1>
	<%
		}
	%>
	<p></p><h1>レア度：${gacha.rarity}</h1>
	<div class=chara>
		<img src="img/${gacha.num}.jpg" alt="" class="rounded-circle"
			width="300" height="350">
	</div>
	<h2>※画像はイメージです</h2>
	<p></p>
	<div class=status>
		<p>${gacha.name}「${gacha.script}」</p>
		<p>キャラ名：${gacha.name} 属性：${gacha.attribute}</p>
		<p>タイプ：${gacha.type}</p>
		<p>HP：${gacha.status[0]} SP：${gacha.status[1]}
			攻撃力：${gacha.status[2]} 防御力：${gacha.status[3]} 魔力：${gacha.status[4]}</p>
		<p>魔防：${gacha.status[5]} 速度：${gacha.status[6]}
			回避：${gacha.status[7]} スキル：${gacha.status[8]}</p>

	</div>
	<div class="css-fukidashi1">
		<p class="text1">スキル1:${gacha.skill1Name}</p>
		<p class="fukidashi1">${gacha.skill1Explanation}</p>
	</div>
	<div class="css-fukidashi2">
		<p class="text2">スキル2:${gacha.skill2Name}</p>
		<p class="fukidashi2">${gacha.skill2Explanation}</p>
	</div>
	<div class="css-fukidashi3">
		<p class="text3">スキル3:${gacha.skill3Name}</p>
		<p class="fukidashi3">${gacha.skill3Explanation}</p>
	</div>

	<p>キャラ説明：${gacha.charaDescription}</p>

	<p>現在所持しているキャラは</p>
	<%
		for (int i = 0; i < gacha.charaKeep.size(); i++) {
	%>
	<p>
		【<%=i%>】<%=gacha.charaKeep.get(i).getName()%></p>
	<%
		}
	%>
	<%
		if (gacha.charaKeep.size() == 0) {
	%>
	<p>0</p>
	<%
		}
	%>
	<p>です。</p>

	<div class=button>
		<form action="/GachaGame/GachaController" method="post">
			<input type="submit" value="続けてガチャを引く">
		</form>


		<form action="/GachaGame/GachaSave" method="post">
			<input type="submit" value="入手済みのキャラクターを表示">
		</form>
	</div>
</body>
</html>
