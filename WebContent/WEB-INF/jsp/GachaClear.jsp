<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="gacha.Gacha" %>
<% Gacha gacha=(Gacha)session.getAttribute("gacha"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
		<h1>指定されたキャラクターを削除しました</h1>
		<p>現在保存されているキャラは</p>
		<% for(int i=0; i<gacha.charaKeep.size();i++){%>
		<p>【<%=i%>】<%=gacha.charaKeep.get(i).getName()%></p>
		<%} %>
		<p>です。</p>
<% if(gacha.charaKeep.size()>1){%>
<form action="/GachaGame/GachaRemove" method="post">
<input type="text" name="number"/>
<input type="submit" value="番目のキャラを続けて削除する">
</form>
<%} %>
<form action="/GachaGame/GachaController" method="post">
<input type="submit" value="続けてガチャを引く">
</form>
</body>
</html>